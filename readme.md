# Arch Linux
### Installation guide for my system

<br>


#### 1. Connect to internet.

###### Setting up enthernet connection via smartphone USB modem or `nmtui`.
###### Checking network connection.
  `ping google.com` _# Checking network connection and dns server._

<br>


#### 2. Activate fish shell.

###### Downloading and start fish.
  `pacman -Sy fish`  
  `fish`

<br>


#### 3. Activating NTP NetworkTimeProtocol.

###### Activating NTP.
  `timedatectl set-ntp true`

<br>


#### 4. Partition setup.

###### List partition with information.
  `fdisk -l`  
  `parted -l`  
  `df -h`  

###### Setting partition table.
  `cfdisk /dev/[device where must install system]`
 > **Create:** efi partition with type: EFI System, size = 100M.  
 > **Create:** swap partition with type: Linux swap, size = RAM .  
 > **Create:** root partition with type: Linux filesystem  

###### Setting up disk encryption  
  `cryptsetup -y luksFormat /dev/[device with root partition]` _# Encrypting root disk partition._  
  `cryptsetup open /dev/nvme0n1p2 cryptroot` _# Open encrypted root disk partition. Last parameter is name._  

###### Formating volumes.
  `mkfs.vfat /dev/[efi partition]` _# Format partition to fat32_  
  `mkfs.ext4 /dev/[partition witch need formating]` _# Format partition to ext4_  
  `mkfs.ext4 /dev/mapper/[root disk partition name (cryptroot)]` _# Format partition to ext4_  

###### Activating swap.
  `mkswap /dev/[partition for swap with type: Linux swap]`  
  `swapon /dev/[partition for swap]`  
  `free -h` _# Checking RAM and SWAP_

<br>


#### 5. Mounting /

###### Mounting / to /mnt.
  `mount /dev/[partition witch will be a /] /mnt`  
  `mount /dev/mapper/cryptroot /mnt`  

<br>


#### 6. Installing arch.

###### Installing base system.
  `pacstrap /mnt/ base base-devel lvm2`

<br>


#### 7. Creating fstab.

###### Generating partition table.
  `genfstab -U /mnt/ >> /mnt/etc/fstab`

<br>


#### 8. Changing root directory.

###### Go to new system.
  `arch-chroot /mnt/`

<br>


#### 9. Downloading required software.

###### Downloading vim and start fish.
  `pacman -Sy fish vim`  
  `fish`

<br>


#### 10. Setting up bootloader.

###### Configure linux bootctl.
  `mount /dev/[efi partition] /boot/` _# Mounting boot partition._  
  `pacman -S linux`  
  `bootctl install` _# Installing bootloader._  
  `cd /boot/loader/`  

  `vim loader.conf` _# Creating loader configuration. Pass for multibooting._  

  ```
  default arch
  #timeout 5 # If timeout needing.
  ```
  
  `vim entries/arch.conf` _# Creating entries configuration._  
  ```
  title Arch
  linux /vmlinuz-linux
  initrd /initramfs-linux.img
  oprions rw cryptdevice=UUID=$ID:cryptroot root=/dev/mapper/cryptroot resume=/dev/mapper/main-swap
  
  [$ID is real ID from comand:]
  cryptsetup luksUUID /dev/nvme0n1p2
  
  [OLD VERSION]
  options root=PARTUUID=[PARTUUID of root partition, without "", like 3c58ba97-1232-0b40-ab78-071f4e6adcb9] rw # :r !blkid
  
  ```

<br>


#### 11. Setting up kernel.  

  ###### Configure hooks for encryption support.
  `vim /etc/mkinitcpio.conf`  
  ```
  # Add "encrypt lvm2"
  # Add "resume" after "lvm2"
  HOOKS=(base udev autodetect modconf block filesystems keyboard encrypt lvm2 resume fsck)
  ```
  `mkinitcpio -p linux`  

<br>


#### 12. Initial setup.

###### Setting host name.
  `echo [hostname] > /etc/hostname`

###### Setting system language.
  `echo 'LANG="en_US.UTF-8"' > /etc/locale.conf`

###### Setting locales.
  `vim /etc/locale.gen` _# Uncoment_ `#en_US.UTF-8 UTF-8` _and_ `#ru_RU.UTF-8 UTF-8`.  
  `locale-gen`

###### Setting timezone and time.
  `ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime` _# Choice your timezone._  
  `hwclock --systohc`

###### Installing initial software.
  `pacman -S sudo linux-firmware`

###### Setting root user.
  `passwd root` _# Creating password for root_.  
  `vim /etc/sudoers` _# Uncoment_ `%wheel ALL=(ALL) ALL`.

###### Setting other users.
  `useradd -m [name of new user]`  
  `passwd [name of new user]`  
  `usermod -a -G wheel [name of new user]`  

###### Setting enother shell.
  `whereis [enother shell 'fish']`  
  `chsh -s [first path '/usr/bin/fish']` # _Perform from all users._

<br>


#### 13. Installing video driver.

###### OR Installing intel video driver.
  `lspci | grep VGA` _# Check graphics card._  
  `pacman -Ss xf86-video` _# Search driver for graphics card._  
  `pacman -S [choised driver like 'xf86-video-intel']` _# Installing driver for graphics card._  

###### OR Installing Nvidia Proprietary driver.
  `pacman -S nvidia nvidia-settings`  

<br>


#### 14. Installing user interface.

###### OR Installing KDE user interface and SDDM display manager.   
  > Plasma can be started without dm with `startplasma-wayland`

  `pacman -Sy plasma-meta sddm`  
  `systemctl enable sddm`  

###### OR Installing KDE user interface and SDDM display manager.
  > Plasma can be started without dm with `gnome-shell --wayland`  

  `pacman -S gnome gdm`  
  `systemctl enable gdm`  
  

###### OR Installing I3 user interface and Ly display manager.
  `pacman -S i3-gaps i3status dmenu slock rofi numlockx feh ntp mpd`  
  `pacman -R dmunu`
  `yay -S ly` _# After installing yaourt repo._  
  `systemctl enable ly`   
  `systemctl enable slock@xai.service` _# Activate screen lock after copy service to `/etc/systemd/system/slock@.service`._  
  `systemctl enable ntpd` 

<br>


#### 15. System setup.

###### Enabling Network manager.
  `pacman -S networkmanager network-manager-applet`  
  `systemctl enable NetworkManager`

###### Installing software.
  `pacman -S openssh ufw unrar htop git vlc gnome-terminal/konsole nautilus/dolphine ntfs-3g`  

###### Setting firewall.
  `systemctl enable ufw`  

###### Creating directories for users. Not required.
  `xdg-user-dirs-update` _# Perform from all users only for KDE._  

<br>


#### 16. Reboot to new system.
###### Exit from fish.
  `exit`

###### Exit from chroot.
  `exit`

###### Unmount all devices.
  `umount -a`

###### Reboot to new system.
  `reboot`
