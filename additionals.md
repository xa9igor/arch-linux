#### Installing YaY.

  `git clone https://aur.archlinux.org/yay-git.git`  
  `cd yay-git`  
  `makepkg -si`  
  `cd ..`  
  `rm -rf yay-git/`  

<br>


#### Installing Additional software.
  `yay -S google-chrome google-chrome-beta pycharm-professional python38 slack-desktop postgresql`  
  `pacman -S telegram-desktop transmission-gtk ntfs-3g `

<br>


#### Bluetooth.
  
  `sudo pacman -S pulseaudio-bluetooth`
  `systemctl enable bluetooth`  
  `systemctl start bluetooth`  

<br>


#### SSH.
 
  `cd .ssh`  
  `chod 600 *`  

<br>


#### Emoji.
  `yay -S noto-fonts-emoji-apple`  

<br>


#### For Varmillo FN-lock keys.
  `echo 2 | sudo tee /sys/module/hid_apple/parameters/fnmode`
  >Check for working. Continue if FN-lock work correctly.

  `touch /etc/ModemManager/hid_apple.conf`  
  `echo options hid_apple fnmode=2 | sudo tee -a /etc/modprobe.d/hid_apple.conf`  
  `mkinitcpio -p linux`  

<br>


#### For incorrect Slack link.

  `while sleep .1; do ps aux | grep slack | grep -v grep | grep magic; done`  

  ```
  Try logging in to slack - accept login attempt in browser
  You should see login link in console:
  /usr/lib/slack/slack --enable-crashpad slack://workspace_id/magic-login/...
  Open the slack with correct link with uppercase workspace_id:
  /usr/lib/slack/slack --enable-crashpad slack://WORKSPACE_ID/magic-login/...
  ```

<br>


#### Terminal themes.
  `bash -c "$(wget -qO- https://git.io/vQgMr)"`
  [List of themes.](https://gogh-co.github.io/Gogh/)

<br>


### Fish greeting.
  ```
  function fish_greeting
      echo "Welcome back XAI"
  end
  funcsave fish_greeting
  ```

<br>


#### Pycharm.

  ###### Themes
  * CodelyTV Theme
  * Dracula Theme
  * Gruvbox Theme
  * Blackbird Theme
  * Carbon
  * Celestial
  * Deep Ocean Theme
  * Iceberg
  * Lotus Theme
  * Nightfall Theme
  * Obsidian

  ###### Plugins

  * CodeGlance Pro
  * Grep COnsole
  * .env files support

<br>


#### Nvidia kernel update hook.

  `vim /etc/pacman.d/hooks/nvidia.hook`  

  ```
  [Trigger]
  Operation=Install
  Operation=Upgrade
  Operation=Remove
  Type=Package
  Target=nvidia
  Target=linux
  # Change the linux part above and in the Exec line if a different kernel is used

  [Action]
  Description=Update NVIDIA module in initcpio
  Depends=mkinitcpio
  When=PostTransaction
  NeedsTargets
  Exec=/bin/sh -c 'while read -r trg; do case $trg in linux) exit 0; esac; done; /usr/bin/mkinitcpio -P'
  ```

<br>


#### Disable window frames
  > KDESettings > Workspace > Window Management > Window Rules > Add New  
  > Description = .*  
  > Windows class = Regular Expression  
  > Add Property: No titlebar and frame > Force.

<br>


#### Scaling normalisation.  
  > Если в мониках скейлинг нормальный 100%, но шрифт все еще большой.   

  Settings > Appearance > Font > Activate Force Font DPI
  
<br>


#### Mouse scrolling speed on X11 (need check)
  `sudo pacman -S xf86-input-evdev`
  Add to `/etc/X11/xorg.conf`

  ```
  Section "InputClass"
      Identifier "evdev-mouse"
      MatchIsPointer "yes"
      Driver "evdev"
  EndSection
  ```

> Вроде как работает в системе, но Хром не подтянулся под эти настройки. Для chrome: Scroll Speed 0.1

<br>
